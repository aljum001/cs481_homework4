﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;


namespace homework4
{
    public class Recipe
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }

    }
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
        public static int state = 0; //state variable holds the state for onappearing
        //observable collection of type Recipe called recipebook.
        public ObservableCollection<Recipe> RecipeBook { get; set; }
		public Page1 ()
		{
			InitializeComponent ();
            getRecipe();
		}
        //Initilizes The listview with 4 cells for each recipe.
        private void getRecipe()
        {
            RecipeBook = new ObservableCollection<Recipe>()
            {
            new Recipe()
            { //Custom cell for KFC
                Name = "KFC",
                Image = "kfc.jpg",
                Color = "Crimson"

            },
            new Recipe()
            {//Custom cell for Popeyes
                Name = "Popeyes",
                Image = "popeyes.jpg",
                Color = "Crimson"
            },
            new Recipe()
            {//Custom cell for Popeyes Chicken Sandwich
                Name = "Popeyes Chicken Sandwich",
                Image = "pSandwich.jpg",
                Color = "Crimson"
            },
            new Recipe()
            {//Custom cell for Church's Chicken
                Name = "Church's Chicken",
                Image = "churchs.jpg",
                Color = "Crimson"
            }
            };
            R.ItemsSource = RecipeBook;
            
        }
        //Function is called when the user holds the cell and chooses an option from the context menu
        void  MenuItem_Clicked(object sender, SelectedItemChangedEventArgs e)
        {
            //Depending on which cell it will push a page onto the screen.
            //var menu = (Recipe) e.SelectedItem;
            var item1 = (MenuItem)sender;
            Recipe item = (Recipe)item1.CommandParameter;
            if (item.Name == "KFC")
            {
                //menu.Color = "Black";
                Navigation.PushAsync(new kfc());
            }
            else if (item.Name == "Popeyes")
            {
                Navigation.PushAsync(new popeyes());
            }
            else if (item.Name == "Popeyes Chicken Sandwich")
            {
                Navigation.PushAsync(new popeyesSandwich());
            }
            else if (item.Name == "Church's Chicken")
            {
                Navigation.PushAsync(new churchs());
            }
        }
        //Refreshing function is used for the pulldown refresh functionality of the app.
        private void R_Refreshing(object sender, EventArgs e)
        {
            getRecipe();
            R.IsRefreshing = false;
            
        }
        //MenuItem_Delete is used when the user tries to delete a cell through it's context menu.
        private async void MenuItem_Delete(object sender, SelectedItemChangedEventArgs e)
        {//displays an alert to ask the user to be sure of their answer and based on their answer it will either delete or not.
            var result = await DisplayAlert("Alert", "Are you sure you want to Delete?", "Yes", "No");
            if (result)
            {
                var item = (MenuItem)sender;
                var delete = (Recipe)item.CommandParameter;
                RecipeBook.Remove(delete);
            }

        }
        //Onappearing function for when the page appears. Displays an alert only once by using the state variable.
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            if(state == 0 )
            {
                await DisplayAlert("Instructions", "Click and hold on each cell to get more info. If you delete one by mistake, refresh the page and it will come back.", "Ok");
                state = 1;
            }
        }
    }
}