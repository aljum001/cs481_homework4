﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace homework4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class churchs : ContentPage
	{
        public static int state = 0;
		public churchs ()
		{
			InitializeComponent ();
		}
        //appearing page that uses a delay and displays an alert only once by using a state variable.
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (state == 0)
            {
                await Task.Delay(500);
                await DisplayAlert("Well", "I have never tried this so don't blame me if it is not good", "Ok");
                state = 1;
            }
        }
    }
}