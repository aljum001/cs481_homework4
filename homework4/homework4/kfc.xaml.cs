﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace homework4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class kfc : ContentPage
    {
        public kfc()
        {
            InitializeComponent();
        }
        //Appearing function delays by 0.3 seconds and rotates the label and picture for kfc page.
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(300);
            L1.RelRotateTo(360);
            I1.RelRotateTo(360);
        }
    }
}