﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace homework4
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        //B1 function is called when the user clicks the button on the main page and it will push the page with the list view.
        public async void B1(Button sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }
    }
}
