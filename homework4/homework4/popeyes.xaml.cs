﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace homework4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class popeyes : ContentPage
    {
        public popeyes()
        {
            InitializeComponent();
        }
        //Appearing function that uses delays and outputs text through a label on popeyes page.
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            L1.Text = "";
            L2.Text = "";
            await Task.Delay(1000);
            L1.Text = "This is my recipe inspired by others.";
            await Task.Delay(200);
            L2.Text = "I made it and it's amazing.";
        }
    }
}